namespace arvoreBinaria
{
    public class arvore
    {
        public readonly no noRaiz;
        
        
        
        public void inserir(int valor)
        {
            no novoNo = new no(valor);
            no cursorAntigo = null;
            no cursor = this.noRaiz;
            novoNo.valor = valor;
            while (cursor != null)
            {
                if (novoNo.valor < cursor.valor)
                {
                    cursorAntigo = cursor;
                    cursor = cursor.Esquerda;
                }

                if (cursor != null && novoNo.valor >= cursor.valor)
                {
                    cursorAntigo = cursor;
                    cursor = cursor.Direita;
                }
                
            }

            if (valor < cursorAntigo.valor)
            {
                cursorAntigo.Esquerda = novoNo;
            }

            if (valor >= cursorAntigo.valor)
            {
                cursorAntigo.Direita = novoNo;
            
            }
            

        }

        public int remover(int valor)
        {
            return 0;
        }

        public no buscar(int valor)
        {
            
            no cursorAntigo = null;
            no cursor = this.noRaiz;
            
            while (cursor != null)
            {
                if (valor == cursor.valor)
                {
                    return cursor;
                }
                if (valor < cursor.valor)
                {
                    //caminha para a Esquerda
                    cursorAntigo = cursor;
                    cursor = cursor.Esquerda;
                }

                if (valor > cursor.valor)
                {
                    // caminha para a Direita
                    cursorAntigo = cursor;
                    cursor = cursor.Direita;
                }
                
            }

            return new no(-1);

        }

        public arvore(int valor)
        {
            noRaiz = new no(valor);
        }
    }
    
}